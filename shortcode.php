<?php

use WordPressPluginRepo\Ramsey\Uuid\Uuid;

if($_GET['auth_token']):

	$token = new WP_Query(array(
		'post_type' => PRR_CPT_PluginRegistration::$post_type,
		'meta_query' => array(
			array(
				'key' => 'key',
				'value' => $_GET['auth_token']
			)
		)
	));
	if(count($token->posts) != 1) {
		return wp_die("Invalid auth token");
	}
	$token = $token->posts[0];

	if($_POST['action'] == 'addlicense') {

		if (
			! isset( $_POST['wprepo_nonce'] )
			|| ! wp_verify_nonce( $_POST['wprepo_nonce'], 'addlicense' )
		) {

			echo '<div class="alert alert-danger">Please try again</div>';

		} else {

			$result = PluginRepoPlugin::register_license($token, $_POST['license']);

			if($result === true) {

				echo '<div class="alert alert-success">' . __('License has been attached to this site') . '</div>';

			} else {

				echo '<div class="alert alert-warning">' . $result . '</div>';

			}

		}

	}

	?>

	<h2><?= __('The current site has the following licenses attached'); ?></h2>
	<?php

	$plugins = get_post_meta($token->ID, 'plugins', true);

	if(!$plugins) {
		echo '<div class="alert alert-info">' . __('No licenses are attached to this site') . '</div>';
	} else {
		$plugins = new WP_Query(array(
			'post_type' => PRR_CPT_Plugin::get_post_type(),
			'post__in' => $plugins
		));
		while($plugins->have_posts()): $plugins->the_post();

			?>
			<div class="card mb-4">
				<div class="card-body row">
					<div class="col-10">
						<p><strong><?= get_the_title(); ?></strong></p>
						<p>
							<?= get_post_meta(get_the_ID(), 'short_description', true); ?>
						</p>
					</div>
					<div class="col-2">
						<img class="w-100" src="<?= get_the_post_thumbnail_url(null, 'wprepo_2x'); ?>" />
					</div>
				</div>
			</div>
			<?php

		endwhile;
		wp_reset_postdata();
	}

	echo '<h2>' . __('Add a new license') . '</h2>';

	?>
	<form method="post">
		<input type="hidden" name="action" value="addlicense" />
		<?php wp_nonce_field('addlicense', 'wprepo_nonce'); ?>

		<div class="form-group mb-3">
			<label for="license" class="form-label"><?= __('License'); ?></label>
			<input type="text" class="form-control" name="license" id="license" />
		</div>

		<button type="submit" class="btn btn-primary"><?= __('Add'); ?></button>

	</form>
	<hr />
	<p>
		<a class="btn btn-primary" href="<?= esc_attr($_GET['return_url']); ?>"><?= __('Return to your dashboard'); ?></a>
	</p>
	<?php


elseif($_GET['return_url']):

	if($_GET['continue']):

		$uuid = Uuid::uuid4();

		// Create registration and store details about it
		$post = wp_insert_post(array(
			'post_type' => PRR_CPT_PluginRegistration::get_post_type(),
			'meta_input' => array(
				'plugins' => '',
				'key' => $uuid->toString(),
			),
			'post_status' => 'publish'
		));

		$url = add_query_arg(array(
			'token' => $uuid->toString(),
		), $_GET['return_url']);

		?>
		<a href="<?= esc_attr($url); ?>" class="redir">If you are not redirected</a>
		<script>
			(function($){
				document.location.href = $(".redir").attr("href");
			})(jQuery);
		</script><?php

	else:
?>

	<div class="alert alert-warning">
		<p><strong><?= __('Consent'); ?></strong></p>
		<p>
			<?= __('By clicking continue you agree that we can store information about what license keys you redeem
			against the site and how you download plugins. This is for the purposes of ensuring license
			keys are used appropriately.'); ?>
		</p>
		<p>
			<?= __('This information does not identify an individual. Any billing information is not stored on
			this system.'); ?>
		</p>
		<a class="btn btn-primary" href="<?= add_query_arg('continue', 'true'); ?>">
			<?= __('Continue'); ?>
		</a>
	</div>

<?php endif; ?>

<?php else: ?>
<div class="alert alert-danger">
    <p><strong>Page not called correctly</strong></p>
    <p>
        This page is used to manage your access to the plugin repo. It appears the page was not called
        with the correct information.
    </p>
</div>
<?php endif; ?>
