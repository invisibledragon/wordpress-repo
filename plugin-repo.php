<?php
/**
 * @package Plugin Repo
 * @version 1.4.2
 */
/*
Plugin Name: Plugin Repo
Plugin URI: https://invisibledragonltd.com/wordpress/repo/
Description: Add-on to allow this WordPress blog to host a plugin repo. Requires MetaBoxes and MB Relationships
Author: Invisible Dragon
Author URI: https://invisibledragonltd.com/
License: TBC
Version: 1.4.2
*/
require 'vendor/autoload.php';

use WordPressPluginRepo\Ramsey\Uuid\Uuid;
use InvisibleDragon\PluginCore\CPT_Post;
use InvisibleDragon\PluginCore\Settings_Page;
use InvisibleDragon\PluginCore\PluginCore;
use InvisibleDragon\PluginCore\Media_Library;

add_action( 'init', array( 'PluginRepoPlugin', 'activate' ) );
add_action( 'admin_menu', array( 'PluginRepoPlugin', 'admin_activate' ) );


class PluginRepoPlugin {

	public static function activate() {
		PRR_CPT_Plugin::activate();
		PRR_CPT_PluginVersion::activate();
		PRR_CPT_PluginRegistration::activate();
		PRR_CPT_PluginLicense::activate();

		PluginCore::activate();

		add_action( 'wp_head', array( static::class, 'add_header_link' ) );
		add_action( 'wp_ajax_nopriv_wprepo', array( static::class, 'get_repo' ) );
		add_action( 'wp_ajax_wprepo', array( static::class, 'get_repo' ) );
		add_action( 'wp_ajax_nopriv_wprepo_gitlab', [ static::class, 'gitlab_webhook' ] );
		add_action( 'wp_ajax_wprepo_gitlab', [ static::class, 'gitlab_webhook' ] );
		add_action( 'wp_ajax_nopriv_wprepo_download', [ static::class, 'get_repo_download' ] );
		add_action( 'wp_ajax_wprepo_download', [ static::class, 'get_repo_download' ] );

		add_filter( 'display_post_states', array( static::class, 'get_post_state' ), 10, 2 );

		add_shortcode( 'wprepo_manage', array( static::class, 'manage_shortcode' ) );

		if(class_exists('WooCommerce')) {
			require_once dirname(__FILE__) . '/plugin-repo-woocommerce.php';
			PluginRepoWoocommerce::activate();
		}

		if(is_admin()) {
			PRR_Settings_Page::activate();
		}

	}

	public static function admin_activate() {
		PRR_Admin::activate();
	}

	public static function add_header_link() {
		echo '<link rel="wp-plugin-repo" href="' . admin_url('admin-ajax.php?action=wprepo') . '" />';
	}

	public static function manage_shortcode( $args ) {

		ob_start();
		require "shortcode.php";
		return ob_get_clean();

	}

	public static function get_post_state( $states, $post ) {

		$options = get_option(PRR_Admin::$option);

		if($options['settings_page'] == $post->ID)
			$states[] = __('Repo Management');

		return $states;

	}

	public static function get_repo_download() {

		if($_GET['token']) {
			$token = new WP_Query(array(
				'post_type' => PRR_CPT_PluginRegistration::get_post_type(),
				'meta_query' => array(
					array(
						'key' => 'key',
						'value' => $_GET['token']
					)
				)
			));
			if(count($token->posts) != 1) {
				return wp_send_json_error([
					"message" => "Invalid auth token"
				]);
			}
			$token = $token->posts[0];

			$token_plugins = get_post_meta($token->ID, 'plugins', true);
			if(!in_array($_GET['plugin_id'], $token_plugins)) {
				return wp_send_json_error([
					"message" => "Plugin not licensed"
				]);
			}
		}

		$version = new WP_Query(array(
			'post_type' => PRR_CPT_PluginVersion::get_post_type(),
			'meta_query' => array(
				array(
					'key' => 'plugin',
					'value' => $_GET['plugin_id']
				)
			),
			'order' => 'DESC',
			'meta_key' => 'version',
			'orderby' => 'meta_value',
			'per_page' => 1,
			'p' => $_GET['version_id']
		));
		$version = $version->posts[0];

		if(!$version) {
			return wp_send_json_error([
				"message" => "Plugin version not available"
			]);
		}

		$file = get_post_meta($version->ID, 'file', true);
		$file = get_post_meta( $file, '_wp_attached_file', true );
		$uploads = wp_get_upload_dir();
		$file = $uploads['basedir'] . '/' . $file;

		header('Content-Disposition: attachment; filename='.basename($file));
		header('Content-Type: application/zip');
		readfile($file);
		exit;

	}

	public static function plugin_to_json($group, $api_token = '') {

		$version = new WP_Query(array(
			'post_type' => PRR_CPT_PluginVersion::get_post_type(),
			'meta_query' => array(
				array(
					'key' => 'plugin',
					'value' => get_the_ID()
				)
			),
			'order' => 'DESC',
			'meta_key' => 'version',
			'orderby' => 'meta_value',
			'per_page' => 1
		));
		$version = $version->posts[0];

		$download_link = add_query_arg([
			'token' => $api_token,
			'plugin_id' => get_the_ID(),
			'version_id' => $version->ID
		], admin_url('admin-ajax.php?action=wprepo_download'));

		return array(
			'name' => get_the_title(),
			'type' => get_post_meta(get_the_ID(), 'type', true),
			'slug' => get_post_meta(get_the_ID(), 'slug', true),
			'short_description' => get_post_meta(get_the_ID(), 'short_description', true),
			'author' => get_option('blogname'),
			'icons' => array(
				'1x' => get_the_post_thumbnail_url(null, 'wprepo_1x'),
				'2x' => get_the_post_thumbnail_url(null, 'wprepo_2x')
			),
			'sections' => array(
				'Description' => apply_filters( 'the_content', get_the_content() ),
				'Release' => apply_filters( 'the_content', get_the_content(null, false, $version) ),
			),
			'banners' => array(
				'low' => wp_get_attachment_image_url(get_post_meta(get_the_ID(), 'banner', true), 'wprepo_low'),
				'high' => wp_get_attachment_image_url(get_post_meta(get_the_ID(), 'banner', true), 'wprepo_high')
			),
			'banner_text_right' => true,
			'group' => $group,
			'buy_link' => get_post_meta(get_the_ID(), 'link', true),
			'version' => get_post_meta($version->ID, 'version', true),
			'download_link' => $download_link,
			'last_updated' => get_the_date('Y-m-d', $version)
		);

	}

	public static function get_repo() {

		$options = PRR_Settings_Page::get_options();
		ini_set('display_errors', 'On');

		$auth = array(
			'available' => true,
			'authenticated' => false,
			'url' => get_the_permalink($options['settings_page'])
		);

		$token = null;

		if(array_key_exists('HTTP_AUTHORIZATION', $_SERVER) && !empty($_SERVER['HTTP_AUTHORIZATION'])) {
			$auth_token = explode(' ', $_SERVER['HTTP_AUTHORIZATION']);

			if(stripos($auth_token[1], 'RL_') === 0) {

				// We need to create an auth token right here, right now
				$j = json_decode(stripslashes(stripslashes(substr($auth_token[1], 3))), true);
				if(!$j) {
					return wp_die("Invalid auth token B");
				}

				$uuid = Uuid::uuid4();

				$post = wp_insert_post(array(
					'post_type' => PRR_CPT_PluginRegistration::$post_type,
					'meta_input' => array(
						'plugins' => '',
						'key' => $uuid->toString(),
					),
					'post_status' => 'publish'
				));

				static::register_license($post, $j['key']);

				$auth_token[1] = $uuid->toString();

			}

			$token = new WP_Query(array(
				'post_type' => PRR_CPT_PluginRegistration::get_post_type(),
				'meta_query' => array(
					array(
						'key' => 'key',
						'value' => $auth_token[1]
					)
				)
			));
			if(count($token->posts) != 1) {
				return wp_send_json_error([
					"message" => "Invalid auth token"
				]);
			}
			$token = $token->posts[0];

			$auth['authenticated'] = true;
			$auth['manage_url'] = add_query_arg('auth_token', $auth_token[1], get_the_permalink($options['settings_page']));
			$auth['token'] = $auth_token[1];
		}

		$repo = array(
			'plugins' => array(),
			'repo' => array(
				'name' => $options['repo_name'],
				'auth' => $auth,
				'url' => admin_url('admin-ajax.php?action=wprepo')
			)
		);

		if($_GET['key']) {

			// So we are gonna do a dry run here!
			if(static::register_license($_GET['product'], $_GET['key'], true)) {
				// If we can potentially register on the next round
				$repo['repo']['auth']['token'] = 'RL_' . json_encode($_GET, true);
				$plugins = new WP_Query(array(
					'post_type' => PRR_CPT_Plugin::get_post_type(),
					'meta_query' => array(
						array(
							'key' => 'product_code',
							'value' => $_GET['product']
						)
					)
				));
				while($plugins->have_posts()) {
					$plugins->the_post();
					$repo['plugins'][] = static::plugin_to_json(__('Will be licensed pluigns'));
				}

			}

		}

		if($token) {

			$token_plugins = get_post_meta($token->ID, 'plugins', true);

			if(!empty($token_plugins)) {

				$plugins = new WP_Query( array(
					'post_type' => PRR_CPT_Plugin::get_post_type(),
					'post__in'  => $token_plugins
				) );

				while ( $plugins->have_posts() ) {
					$plugins->the_post();
					$j                    = static::plugin_to_json( __( 'Licensed Plugins' ), $auth['token'] );
					$j['plugin_licensed'] = true;
					$repo['plugins'][]    = $j;
				}

			}

		}

		$plugins = new WP_Query(array(
			'post_type' => PRR_CPT_Plugin::get_post_type(),
			'meta_query' => array(
				array(
					'key' => 'public_download',
					'value' => '1'
				)
			)
		));

		while($plugins->have_posts()) {
			$plugins->the_post();
			$repo['plugins'][] = static::plugin_to_json(__('Public Plugins'));
		}


		header("Content-Type: application/json");
		echo json_encode($repo);

		wp_die();
	}

	public static function register_license($key, $license_key, $dry_run = false) {

		$license = new WP_Query(array(
			'post_type' => PRR_CPT_PluginLicense::get_post_type(),
			'meta_query' => array(
				array(
					'key' => 'key',
					'value' => $license_key
				)
			)
		));
		if(count($license->posts) != 1){ return 'Plugin could not be found'; }
		$license = $license->posts[0];

		$uses = new WP_Query([
			'post_type' => PRR_CPT_PluginRegistration::get_post_type(),
			'meta_query' => [
				[
					'key' => 'plugins',
					'value' => get_post_meta($license->ID, 'plugin', true),
					'compare' => 'IN'
				]
			]
		]);
		$total_uses = get_post_meta($license->ID, 'uses', true);

		if($uses->found_posts >= $total_uses) {
			return "License has exceeded the number of valid activations. Please purchase additional licenses";
		}

		$plugins = get_post_meta($key->ID, 'plugins', true);
		if(is_string($plugins)) {
			$plugins = [];
		}
		$plugins[] = get_post_meta($license->ID, 'plugin', true);
		$plugins = array_unique($plugins);
		update_post_meta($key->ID, 'plugins', $plugins);

		return true;

	}

	/**
	 * Takes in from Gitlab
	 */
	public static function gitlab_webhook() {

		$options = PRR_Settings_Page::get_options();
		if($options['gitlab_key']) {
			if($_SERVER['HTTP_X_GITLAB_TOKEN'] !== $options['gitlab_webhook_secret']) {
				wp_die('Unauthenticated Request');
			}
		}

		// Deal with webhook
		$json = file_get_contents('php://input');
		$obj = json_decode($json, true);

		if($obj['object_kind'] == 'pipeline') {

			// Pipeline event!
			if($obj['object_attributes']['status'] == 'success') {

				$jobs = [];
				foreach($obj['builds'] as $job) {
					$jobs[] = $job['name'];
				}
				$query = new WP_Query([
					'post_type' => PRR_CPT_Plugin::get_post_type(),
					'per_page' => 1,
					'meta_query' => [
						'relation' => 'AND',
						[
							'key' => 'gitlab_repo',
							'value' => $obj['project']['path_with_namespace']
						],
						[
							'key' => 'gitlab_job',
							'compare' => 'IN',
							'value' => $jobs
						]
					]
				]);
				foreach($query->posts as $post) {

					// Add artifact
					$post_id = $post->ID;
					$artifact = get_post_meta($post_id, 'gitlab_artifact', true);
					$repo = urlencode(get_post_meta($post_id, 'gitlab_repo', true));
					$job_name = get_post_meta($post_id, 'gitlab_job', true);
					$type = get_post_meta($post_id, 'type', true);
					$slug = get_post_meta($post_id, 'slug', true);
					$job_id = -1;
					foreach($obj['builds'] as $job) {
						if($job['name'] == $job_name) {
							$job_id = $job['id'];
						}
					}
					$url = PluginCore::deslash_host($options['gitlab_server']) .
					       '/api/v4/projects/' . $repo . '/jobs/' . $job_id . '/artifacts/' . $artifact;
					$resp = wp_remote_get($url, [
						'headers' => [
							'Authorization' => 'Bearer ' . $options['gitlab_key']
						]
					]);
					if($resp['response']['code'] != 200) {
						return wp_die('Cannot login to gitlab');
					}
					$media = Media_Library::upload_file($resp['body'], str_replace('/', '_', $artifact), true);
					$file = 'zip://' . $media[0] . '#' . $slug;
					if($type === 'theme') {
						$file .= '/style.css';
					}
					$headers = get_file_data($file, [
						'Version' => 'Version'
					], $type);

					// Check if the version is already uploaded
					$query = new WP_Query([
						'post_type' => PRR_CPT_PluginVersion::get_post_type(),
						'meta_query' => [
							[
								'key' => 'plugin',
								'value' => $post_id
							]
						],
						'date_query' => array(
							array(
								'column' => 'post_date_gmt',
								'before'  => '90 days ago',
							)
						)
					]);
					if(count($query->posts) > 0) {
						// If so, delete the attached file
						foreach($query->posts as $old) {
							wp_trash_post( $old->ID );
						}
					}

					wp_insert_post([
						'post_type' => PRR_CPT_PluginVersion::get_post_type(),
						'post_status' => 'publish',
						'post_title' => $headers['Version'],
						'meta_input' => [
							'plugin' => $post_id,
							'version' => $headers['Version'],
							'file' => $media[1]
						]
					]);

				}

			}

		}

	}

}


class PRR_Admin {

	public static $option = 'wprepo_settings';

	public static function activate() {

		static::customise_admin_menu();

	}

	public static function customise_admin_menu() {

		global $menu;

		// Change main menu item
		foreach($menu as $i => &$item) {
			if( $item[2] == 'edit.php?post_type=' . PRR_CPT_Plugin::get_post_type() ) {
				$item[0] = __('Plugin Repo');
			}
		}

	}

}

class PRR_Settings_Page extends Settings_Page {

	public static function get_fields() {
		return [
			'repo_name' => [
				'type' => 'text',
				'title' => __('Repo Name')
			],
			'settings_page' => [
				'type' => 'post',
				'title' => __('Settings Page'),
				'query' => [
					'post_type' => 'page'
				]
			],
			'gitlab_webhook_url' => [
				'type' => 'readonly',
				'title' => __('GitLab Webhook URL'),
				'value' => [ static::class, 'get_webhook_url' ]
			],
			'gitlab_server' => [
				'type' => 'text',
				'title' => __('GitLab Server URL'),
				'default' => 'https://gitlab.com/'
			],
			'gitlab_webhook_secret' => [
				'type' => 'text',
				'title' => __('Gitlab Webhook Secret'),
				'description' => __('If you are using the Gitlab webhooks integration, enter your shared webhook secret here')
			],
			'gitlab_key' => [
				'type' => 'text',
				'title' => __('Gitlab Key'),
				'description' => __('If you are using the Gitlab webhooks integration, enter your API key here')
			]
		];
	}

	public static function get_webhook_url() {
		return add_query_arg('action', 'wprepo_gitlab', admin_url('admin-ajax.php'));
	}

	public static function get_title() {
		return __( 'Repo Settings', 'wprepo' );
	}

	public static function get_menu_slug() {
		return 'wprepo_settings';
	}

	public static function get_setting_key() {
		return 'wprepo_settings';
	}
}

class PRR_CPT_PluginLicense extends CPT_Post {

	public static $post_type = 'wprepo_registration';

	public static function get_post_type():string {
		return 'wprepo_license';
	}

	public static function get_name():string {
		return __( 'Plugin License', 'wprepo' );
	}

	public static function get_tabs() {
		return [
			[
				'key' => 'general',
				'label' => __('General', 'wprepo'),
				'fields' => [
					'key' => [
						'type' => 'text',
						'title' => __('Key'),
						'default' => [ 'WordPressPluginRepo\Ramsey\Uuid\Uuid', 'uuid4' ]
					],
					'plugin' => [
						'type' => 'post',
						'title' => __('Plugin'),
						'query' => [
							'post_type' => PRR_CPT_Plugin::get_post_type()
						]
					],
					'uses' => [
						'type' => 'number',
						'title' => __('Total Uses'),
						'description' => __('Total Number of Uses this license can be used for')
					],
					'notes' => [
						'type' => 'text',
						'title' => __('Notes')
					]
				]
			]
		];
	}

	public static function get_parent_menu_item() {
		return PRR_CPT_Plugin::get_parent_menu_item();
	}

	public static function get_supports() {
		return [''];
	}

	public static function get_columns() {

		return [
			'cb' => static::COLUMN_CHECKBOX,
			'custom_key' => __('Key'),
			'custom_plugin' => __('Plugin'),
			'custom_notes' => __('Notes'),
			'author' => __('Owner')
		];

	}

}

class PRR_CPT_PluginRegistration extends CPT_Post {

	public static $post_type = 'wprepo_registration';

	public static function get_post_type():string {
		return static::$post_type;
	}

	public static function get_name():string {
		return __( 'Plugin Registration', 'wprepo' );
	}

	public static function get_tabs() {
		return [
			[
				'key' => 'general',
				'label' => __('General', 'wprepo'),
				'fields' => [
					'key' => [
						'type' => 'text',
						'title' => __('Key')
					],
					'notes' => [
						'type' => 'text',
						'title' => __('Notes')
					],
					'plugins' => [
						'type' => 'posts',
						'title' => __('Plugins'),
						'query' => [
							'post_type' => PRR_CPT_Plugin::get_post_type()
						]
					]
				]
			]
		];
	}

	public static function get_parent_menu_item() {
		return PRR_CPT_Plugin::get_parent_menu_item();
	}

	public static function get_supports() {
		return [''];
	}

	public static function get_columns() {

		return [
			'cb' => static::COLUMN_CHECKBOX,
			'custom_key' => __('Key'),
			'custom_notes' => __('Notes'),
			'author' => __('Owner')
		];

	}

}


class PRR_CPT_PluginVersion extends CPT_Post {

	public static function get_post_type(): string {
		return 'wprepo_version';
	}

	public static function get_name(): string {
		return __('Plugin Version');
	}

	public static function get_parent_menu_item() {
		return PRR_CPT_Plugin::get_parent_menu_item();
	}

	public static function get_columns() {

		return [
			'cb' => static::COLUMN_CHECKBOX,
			'title' => __('Title'),
			'custom_version' => __('Version'),
			'custom_plugin' => __('Plugin')
		];

	}

	public static function get_tabs() {
		return [
			[
				'key' => 'general',
				'label' => __('General', 'wprepo'),
				'fields' => [
					'plugin' => [
						'type' => 'post',
						'title' => __('Plugin'),
						'query' => [
							'post_type' => PRR_CPT_Plugin::get_post_type()
						]
					],
					'version' => [
						'type' => 'text',
						'title' => __('Version')
					],
					'file' => [
						'type' => 'file',
						'title' => __('File'),
						'private' => true
					]
				]
			]
		];
	}

}

class PRR_CPT_Plugin extends CPT_Post {

	public static function activate() {

		parent::activate();

		add_image_size( 'wprepo_1x', 128, 128, true );
		add_image_size( 'wprepo_2x', 256, 256, true );
		add_image_size( 'wprepo_low', 772, 250, true );
		add_image_size( 'wprepo_high', 1544, 500, true );

	}

	public static function get_tabs() {
		return [
			[
				'key' => 'general',
				'label' => __('General', 'wprepo'),
				'icon' => 'dashicons-admin-tools',
				'fields' => [
					'type' => [
						'type' => 'select',
						'options' => [
							'plugin' => __('Plugin'),
							'theme' => __('Theme')
						],
						'title' => __('Type'),
						'default' => 'plugin'
					],
					'slug' => [
						'type' => 'text',
						'title' => __('Slug')
					],
					'short_description' => [
						'type' => 'text',
						'title' => __('Short Description')
					],
					'banner' => [
						'type' => 'image',
						'title' => __('Banner Image')
					],
					'public_download' => [
						'type' => 'checkbox',
						'title' => __('Public Download')
					]
				]
			],
			[
				'key' => 'gitlab',
				'label' => __('GitLab'),
				'icon' => 'dashicons-admin-site-alt2',
				'fields' => [
					'gitlab_repo' => [
						'type' => 'text',
						'title' => __('Repository')
					],
					'gitlab_job' => [
						'type' => 'text',
						'title' => __('Job Name')
					],
					'gitlab_artifact' => [
						'type' => 'text',
						'title' => __('Artifact Path'),
						'description' => __('Path to the zip file inside of the artifacts generated by the job in question')
					]
				]
			]
		];
	}

	public static function get_supports() {
		return [ static::FEATURE_TITLE, static::FEATURE_FEATURED_IMAGE ];
	}

	public static function get_post_type(): string {
		return 'wprepo_plugin';
	}

	public static function get_name(): string {
		return __('Plugins');
	}
}
