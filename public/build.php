<?php
$file = file_get_contents("repo.json");
$file = str_replace("{{domain}}", $_ENV['CI_PAGES_URL'], $file);
$file = str_replace("{{date}}", date('Y-m-d'), $file);

$vfile = file_get_contents("../plugin-repo.php");
$vfile = explode("\n", $vfile);
$version = '';
$header = ' * @version ';
foreach ($vfile as $line) {
	if(stripos($line, $header) !== FALSE) {
		$version = substr($line, strlen($header));
	}
}
$file = str_replace("{{version}}", $version, $file);

file_put_contents("repo.json", $file);