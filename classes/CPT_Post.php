<?php

namespace InvisibleDragon\PluginCore;

/***
 * This class defines a custom post type for WordPress
 *
 * Make sure activate() is called or it won't be registered
 *
 * @package InvisibleDragon\PluginCore
 */
abstract class CPT_Post {

	/**
	 * The internal name for the post type
	 * @return string
	 */
	abstract public static function get_post_type(): string;

	/**
	 * The label for the post type
	 * @return string
	 */
	abstract public static function get_name(): string;

	public const FEATURE_TITLE = 'title';
	public const FEATURE_EDITOR = 'editor';
	public const FEATURE_FEATURED_IMAGE = 'thumbnail';
	public const COLUMN_CHECKBOX = '<input type="checkbox" />';

	public static function get_supports() {
		return [ static::FEATURE_TITLE, static::FEATURE_EDITOR ];
	}

	static function _default_parent_menu_item() {
		return 'edit.php?post_type=' . static::get_post_type();
	}

	/**
	 * This allows you to pick a different item for this post type to sit under
	 * @return string|null
	 */
	public static function get_parent_menu_item() {
		return static::_default_parent_menu_item();
	}

	public static function get_labels()
	{

		$type = static::get_name();

		if ( substr( $type, -1, 1 ) === 'y' ) {
			$plural_type = substr( $type, 0, -1 ) . 'ies';
		}
		else {
			$plural_type = $type . 's';
		}

		return array( 'name' => __( "{$plural_type}" ), 'singular_name' => __( $type ),
		              'add_new' => __( "Add {$type}" ), 'add_new_item' => __( "Add New {$type}" ),
		              'edit_item' => __( "Edit {$type}" ), 'new_item' => __( "New {$type}" ), 'view_item' => __( "View {$type}" ),
		              'search_items' => __( "Search {$plural_type}" ), 'not_found' => __( "No {$plural_type} found" ),
		              'not_found_in_trash' => __( "No {$plural_type} found in Trash" ), 'parent_item_colon' => '' );

	}

	/**
	 * Get the arguments for this post type to be registered. By default other
	 * functions add information into this array
	 *
	 * @return array
	 */
	public static function get_args() {
		return array(
			'label'                 => static::get_name(),
			'labels'                => static::get_labels(),
			'supports'              => static::get_supports(),
			'taxonomies'            => array(),
			'menu_icon'             => 'dashicons-admin-plugins',
			'capability_type'       => 'page',
			'public' => false,
			'show_ui' => true
		);
	}

	/**
	 * Register the post type into WordPress
	 */
	public static function activate() {

		$args = static::get_args();
		register_post_type( static::get_post_type(), $args );

		// Activate metabox
		static::activate_meta();

		// If non-default parent menu item, let us change it!
		if(static::get_parent_menu_item() != static::_default_parent_menu_item()) {
			static::activate_non_default_parent_menu_item();
		}

		// Activate column changes
		add_filter( 'manage_' . static::get_post_type() . '_posts_columns', [ static::class, 'get_columns' ] );
		add_filter( 'manage_' . static::get_post_type() . '_posts_custom_column', [ static::class, 'get_custom_column' ], 10, 2 );

	}

	public static function find_setting( $key ) {
	    $tabs = static::get_tabs();
	    foreach($tabs as $tab) {
	        foreach($tab['fields'] as $k => $v) {
	            if($k === $key) return $v;
            }
        }
	    return [];
    }

	public static function get_custom_column( $column_key, $post_id ) {

	    if(stripos($column_key, 'custom') === 0) {
	        $key = substr($column_key, 7);
	        $value = get_post_meta($post_id, $key, true);
	        $setting = static::find_setting($key);
	        switch($setting['type']) {
                case 'post':
                    echo '<a href="' . admin_url('post.php?post=' . esc_attr($value) . '&action=edit') . '">';
                    echo get_the_title($value) ?: esc_html($value);
                    echo '</a>';
                    break;
                default:
    	            echo esc_html( $value );
	        }
        }

    }

	public static function get_columns() {

		return [
			'cb' => static::COLUMN_CHECKBOX,
			'title' => __('Title'),
			'date' => __('Date')
		];

	}

	public static function activate_non_default_parent_menu_item() {
		add_action( 'admin_head', [ static::class, 'choose_parent_file' ] );
		add_action( 'admin_menu', [ static::class, 'admin_menu' ] );
	}

	public static function admin_menu() {

		global $menu;
		global $submenu;

		// Remove top-level menu item
		foreach($menu as $i => $item) {
			if($item[2] == static::_default_parent_menu_item()) {
				unset($menu[$i]);
			}
		}

		// Put the contents under the other one
		$submenu[ static::get_parent_menu_item() ] = array_merge(
			$submenu[ static::get_parent_menu_item() ],
			$submenu[ static::_default_parent_menu_item() ]
		);

	}

	public static function choose_parent_file() {
		global $parent_file;

		if($parent_file == 'edit.php?post_type=' . static::get_post_type()) {
			$parent_file = static::get_parent_menu_item();
		}
	}

	public static function activate_meta() {

		add_action( 'add_meta_boxes', [ static::class, 'add_meta_boxes' ] );
		add_filter( 'plugincore_query_cpt_' . static::get_post_type(), [ static::class, 'get_field_query' ], 10, 2 );
		add_action( 'save_post', [ static::class, 'save_fields' ] );

	}

	public static function save_fields( $post_id ) {

		// Verification of post
		if(!isset($_POST['metabox_' . static::get_post_type() . '_nonce'])) return $post_id;
		$nonce = $_POST['metabox_' . static::get_post_type() . '_nonce'];
		if(!wp_verify_nonce($nonce, 'metabox_' . static::get_post_type() . '_fields')) return $post_id;

		// Skip on autosave
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}

		// Now actually set the metadata
		$tabs = static::get_tabs();
		$values = array();
		foreach($tabs as $tab) {
			$settings = new Settings_Fields($tab['fields']);
			$values = array_merge($values, $settings->get_values($_POST));
		}

		foreach($values as $key => $value) {
			update_post_meta($post_id, $key, $value);
		}

		return $post_id;

	}

	public static function get_field_query( $result, $key ) {

		$tabs = static::get_tabs();
		foreach($tabs as $tab) {
			if($tab['fields'][$key]) {
				return $tab['fields'][$key]['query'];
			}
		}
		return $result;

	}

	public static function add_meta_boxes() {
		add_meta_box(
			'cpt_post_data_' . static::get_post_type(),
			static::get_name(),
			[ static::class, 'meta_box_callback' ],
			static::get_post_type(),
			'normal',
			'default'
		);
		add_filter( 'postbox_classes_' . static::get_post_type() . '_cpt_post_data_' . static::get_post_type(), [ static::class, 'add_metabox_class' ] );
	}

	public static function add_metabox_class($classes) {
		$classes[] = 'plugincore-metabox';
		return $classes;
	}

	public static function get_tabs() {
		return [
			[
				'key' => 'general',
				'label' => 'General',
				'fields' => [
					'title' => array(
						'title' => 'Example',
						'type' => 'text',
						'description' => 'This is an example',
						'default' => 'Cheese'
					)
				]
			]
		];
	}

	public static function meta_box_callback($post) {

		$tabs = static::get_tabs();
		$raw_values = get_post_meta($post->ID);
		$values = [];
		foreach($raw_values as $key => $r) {
			$values[$key] = maybe_unserialize($r[0]);
		}

		PluginCore::add_admin_css();
		PluginCore::add_admin_js();

		wp_nonce_field( 'metabox_' . static::get_post_type() . '_fields', 'metabox_' . static::get_post_type() . '_nonce' );

		echo '<div class="plugincore-tab-container">';

		if(count($tabs) > 1):
			?>
            <ul class="plugincore-tabs">
				<?php
				foreach($tabs as $tab):
					?>
                    <li>
                        <a href="#cpt_meta_<?= esc_attr($tab['key']); ?>">
							<?php
							$icon = $tab['icon'] ?: '';
							if ( 0 === strpos( $icon, 'dashicons-' ) ) {
								echo '<i class="' . $icon . ' dashicons"></i>';
							}
							?>
							<?= esc_html($tab['label']); ?>
                        </a>
                    </li>
				<?php endforeach; ?>
            </ul>
		<?php
		endif;

		echo '<div class="plugincore-panel">';
		foreach($tabs as $tab): ?>
            <div class="plugincore-panel-tab" id="cpt_meta_<?= esc_attr($tab['key']); ?>">
				<?php
				$settings = new Settings_Fields($tab['fields'], $values);
				$settings->set_auth_path('cpt_' . static::get_post_type());
				$settings->generate_settings_html();
				?>
            </div>
		<?php

		endforeach;
		echo '</div>';

		echo '<div class="clear"></div></div>';

	}

}

