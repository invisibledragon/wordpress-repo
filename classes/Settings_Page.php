<?php

namespace InvisibleDragon\PluginCore;

abstract class Settings_Page {

	public abstract static function get_fields();

	public abstract static function get_title();

	public abstract static function get_menu_slug();

	public abstract static function get_setting_key();

	public static function activate() {
		add_action( 'admin_menu', [ static::class, 'init' ] );
		add_filter( 'plugincore_query_settings_' . static::get_menu_slug(), [ static::class, 'get_field_query' ], 10, 2 );
	}

	public static function init() {
		add_options_page(
			static::get_title(),
			static::get_title(),
			'manage_options',
			static::get_menu_slug(),
			[ static::class, 'render_settings_page_content' ]
		);
	}

	public static function get_field_query( $result, $key ) {

		$fields = static::get_fields();
		if($fields[$key]) {
			return $fields[$key]['query'];
		}
		return $result;

	}

	public static function get_options() {
		return get_option(static::get_setting_key());
	}

	public static function render_settings_page_content() {
		echo '<div class="wrap" style="max-width: 50rem"><form method="post">';

		echo '<h1>' . esc_html(static::get_title()) . '</h1>';

		PluginCore::add_admin_css();
		PluginCore::add_admin_js();

		$options = static::get_options();
		$settings = new Settings_Fields(static::get_fields(), $options);

		if($_POST) {
			$options = $settings->get_values($_POST);
			update_option(static::get_setting_key(), $options, false);
			echo '<div class="notice notice-success"><p>' . __('Settings Saved') . '</p></div>';
		}

		$settings->set_auth_path('settings_' . static::get_menu_slug());
		$settings->generate_settings_html();

		submit_button();

		echo '</form></div>';
	}

}