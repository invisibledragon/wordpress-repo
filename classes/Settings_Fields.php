<?php

namespace InvisibleDragon\PluginCore;

/**
 * Settings API which allows use of WooCommerce style settings fields without Woo
 * 
 * Some of this code is from WooCommerce itself
 * 
 * @package InvisibleDragon\PluginCore
 */
class Settings_Fields {

	private $form_fields;
	private $values;
	private $prefix = 'pcmeta_';
	private $auth_path = '';

	public function __construct($form_fields, $values = array()) {
		$this->form_fields = $form_fields;
		$this->values = $values;
	}

	public function get_values( $post_data ) {
		foreach ( $this->form_fields as $key => $field ) {
		    $this->values[ $key ] = $this->get_field_value($key, $field, $post_data);
		}

		return $this->values;
    }

	/**
	 * Get a field's posted and validated value.
	 *
	 * @param string $key Field key.
	 * @param array  $field Field array.
	 * @param array  $post_data Posted data.
	 * @return string
	 */
	public function get_field_value( $key, $field, $post_data = array() ) {
		$type      = $this->get_field_type( $field );
		$field_key = $this->get_field_key( $key );
		$post_data = empty( $post_data ) ? $_POST : $post_data; // WPCS: CSRF ok, input var ok.
		$value     = isset( $post_data[ $field_key ] ) ? $post_data[ $field_key ] : null;

		if ( isset( $field['sanitize_callback'] ) && is_callable( $field['sanitize_callback'] ) ) {
			return call_user_func( $field['sanitize_callback'], $value );
		}

		// Look for a validate_FIELDID_field method for special handling.
		if ( is_callable( array( $this, 'validate_' . $key . '_field' ) ) ) {
			return $this->{'validate_' . $key . '_field'}( $key, $value );
		}

		// Look for a validate_FIELDTYPE_field method.
		if ( is_callable( array( $this, 'validate_' . $type . '_field' ) ) ) {
			return $this->{'validate_' . $type . '_field'}( $key, $value );
		}

		// Fallback to text.
		return $this->validate_text_field( $key, $value );
	}

	/**
	 * Validate Text Field.
	 *
	 * Make sure the data is escaped correctly, etc.
	 *
	 * @param  string $key Field key.
	 * @param  string $value Posted Value.
	 * @return string
	 */
	public function validate_text_field( $key, $value ) {
		$value = is_null( $value ) ? '' : $value;
		return wp_kses_post( trim( stripslashes( $value ) ) );
	}

	public function validate_posts_field( $key, $value ) {
	    return explode("," ,$value);
    }

	/**
     * Sets the authentication path which is used in follow-up AJAX calls
     * to see if the request is allowed
     *
	 * @param $auth_path
	 */
	public function set_auth_path($auth_path) {
	    $this->auth_path = $auth_path;
    }

	/**
	 * Get a fields type. Defaults to "text" if not set.
	 *
	 * @param  array $field Field key.
	 * @return string
	 */
	public function get_field_type( $field ) {
		return empty( $field['type'] ) ? 'text' : $field['type'];
	}

	/**
	 * Prefix key for settings.
	 *
	 * @param  string $key Field key.
	 * @return string
	 */
	public function get_field_key( $key ) {
		return $this->prefix . '_' . $key;
	}

	/**
	 * Generate Settings HTML.
	 *
	 * Generate the HTML for the fields on the "settings" screen.
	 *
	 * @param bool  $echo Echo or return.
	 * @return string the html for the settings
	 * @since  1.0.0
	 * @uses   method_exists()
	 */
	public function generate_settings_html( $echo = true ) {

		$html = '';
		foreach ( $this->form_fields as $k => $v ) {
			$type = $this->get_field_type( $v );

			if ( method_exists( $this, 'generate_' . $type . '_html' ) ) {
				$html .= $this->{'generate_' . $type . '_html'}( $k, $v );
			} else {
				$html .= $this->generate_text_html( $k, $v );
			}
		}

		if ( $echo ) {
			echo $html; // WPCS: XSS ok.
		} else {
			return $html;
		}

	}

	public function generate_post_html( $key, $data ) {

		$field_key = $this->get_field_key( $key );
		$value = $this->get_option( $key );

		if($value) {
			$item = PluginCore::query_to_json(array_merge($data['query'], [
				'post__in' => [$value]
			]))[0];
		} else {
			$item = null;
		}

		wp_enqueue_script('jquery-ui-autocomplete');

		ob_start();
		?>
        <div class="form-field plugincore-form-field">
            <label for="<?php echo esc_attr( $field_key ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
            <fieldset class="plugincore-select" data-item="<?= esc_attr(json_encode($item)); ?>" data-auth-path="<?= esc_attr($this->auth_path); ?>" data-key="<?= esc_attr($key); ?>">
                <div class="plugincore-display">
                    <span class="plugincore-label"></span>
                    <a href="#" class="button upload_file_button"><?= __('Select Item'); ?></a>
                </div>
                <input type="text" class="input-text regular-input" placeholder="<?= __('Select Item'); ?>" value="<?= esc_attr($item['label']); ?>" />
                <input id="<?php echo esc_attr( $field_key ); ?>" class="plugincore-raw-value" type="hidden" name="<?php echo esc_attr( $field_key ); ?>" value="<?php echo esc_attr( $value ); ?>" />
            </fieldset>
	        <?php echo $this->get_description_html( $data ); // WPCS: XSS ok. ?>
        </div>
		<?php

		return ob_get_clean();

    }

	public function generate_posts_html( $key, $data ) {

		$field_key = $this->get_field_key( $key );
		$value = $this->get_option( $key );

		if($value) {
			$items = PluginCore::query_to_json(array_merge($data['query'], [
				'post__in' => $value
			]));
		} else {
		    $items = [];
		    $value = [];
        }

		// Add scripts needed for this item
		wp_enqueue_script('jquery-ui-autocomplete');
		wp_enqueue_script('jquery-ui-sortable');

		ob_start();
		?>
        <div class="form-field plugincore-form-field">
            <label for="<?php echo esc_attr( $field_key ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
            <fieldset class="plugincore-select-multiple" data-items="<?= esc_attr(json_encode($items)); ?>" data-auth-path="<?= esc_attr($this->auth_path); ?>" data-key="<?= esc_attr($key); ?>">
                <ul class="plugincore-item-list"></ul>
                <div class="clear"></div>
                <input type="text" class="input-text regular-input" placeholder="<?= __('Add Item'); ?>" />
                <input class="plugincore-raw-value" type="hidden" name="<?php echo esc_attr( $field_key ); ?>" value="<?php echo esc_attr( implode(",", $value) ); ?>" />
            </fieldset>
	        <?php echo $this->get_description_html( $data ); // WPCS: XSS ok. ?>
        </div>
        <?php

        return ob_get_clean();

    }

	public function generate_select_html( $key, $data ) {
		$field_key = $this->get_field_key( $key );
		$defaults  = array(
			'title'             => '',
			'disabled'          => false,
			'class'             => '',
			'css'               => '',
			'placeholder'       => '',
			'type'              => 'text',
			'desc_tip'          => false,
			'description'       => '',
			'custom_attributes' => array(),
			'options'           => [],
			'value'             => $this->get_option( $key ) ?: $data['default']
		);

		$data = wp_parse_args( $data, $defaults );
		$options = PluginCore::value_or_call($data['options']);
		$selected_key = PluginCore::value_or_call( $data['value'] );

		ob_start();
		?>
        <div class="form-field plugincore-form-field">
            <label for="<?php echo esc_attr( $field_key ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
            <select class="regular-input <?php echo esc_attr( $data['class'] ); ?>"
                   type="<?php echo esc_attr( $data['type'] ); ?>"
                   name="<?php echo esc_attr( $field_key ); ?>"
                   id="<?php echo esc_attr( $field_key ); ?>"
                   style="<?php echo esc_attr( $data['css'] ); ?>"
                   placeholder="<?php echo esc_attr( $data['placeholder'] ); ?>"
				<?php disabled( $data['disabled'], true ); ?>
				<?php echo $this->get_custom_attribute_html( $data ); // WPCS: XSS ok. ?>>
                <?php foreach($options as $key => $value): ?>
                    <option value="<?= esc_attr($key); ?>" <?php if($selected_key === $key) echo 'selected="selected"'; ?>><?= esc_html($value); ?></option>
                <?php endforeach; ?>
            </select>
			<?php echo $this->get_description_html( $data ); // WPCS: XSS ok. ?>
        </div>
		<?php

		return ob_get_clean();
	}

	public function generate_text_html( $key, $data ) {
		$field_key = $this->get_field_key( $key );
		$defaults  = array(
			'title'             => '',
			'disabled'          => false,
			'class'             => '',
			'css'               => '',
			'placeholder'       => '',
			'type'              => 'text',
			'desc_tip'          => false,
			'description'       => '',
			'custom_attributes' => array(),
            'value'             => $this->get_option( $key ) ?: $data['default']
		);

		$data = wp_parse_args( $data, $defaults );

		ob_start();
		?>
        <div class="form-field plugincore-form-field">
            <label for="<?php echo esc_attr( $field_key ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
            <input class="input-text regular-input <?php echo esc_attr( $data['class'] ); ?>"
                   type="<?php echo esc_attr( $data['type'] ); ?>"
                   name="<?php echo esc_attr( $field_key ); ?>"
                   id="<?php echo esc_attr( $field_key ); ?>"
                   style="<?php echo esc_attr( $data['css'] ); ?>"
                   value="<?php echo esc_attr( PluginCore::value_or_call( $data['value'] ) ); ?>"
                   placeholder="<?php echo esc_attr( $data['placeholder'] ); ?>"
	            <?php disabled( $data['disabled'], true ); ?>
	            <?php echo $this->get_custom_attribute_html( $data ); // WPCS: XSS ok. ?>>
	        <?php echo $this->get_description_html( $data ); // WPCS: XSS ok. ?>
        </div>
		<?php

		return ob_get_clean();
	}

	public function generate_image_html( $key, $data ) {
	    return $this->generate_file_html( $key, $data );
	}

	public function generate_file_html( $key, $data ) {

		$field_key = $this->get_field_key( $key );
		$value = $this->get_option( $key );

		$attachment = null;
		if($value) {
		    $attachment = get_post($value);
		    $attachment = $attachment->post_title;
		}

		ob_start();

		?>
        <div class="form-field plugincore-form-field">
            <label for="<?php echo esc_attr( $field_key ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
            <fieldset class="plugincore-media" data-type="<?= esc_attr($data['type']); ?>" data-private="<?= esc_attr($data['private'] === true); ?>">
                <div class="plugincore-selected-file"><?= $attachment; ?></div>
                <a href="#" class="button upload_file_button"><?= __('Pick File'); ?></a>
                <input class="plugincore-raw-value" type="hidden" name="<?php echo esc_attr( $field_key ); ?>" value="<?php echo esc_attr( $value ); ?>" />
            </fieldset>
			<?php echo $this->get_description_html( $data ); // WPCS: XSS ok. ?>
        </div>
		<?php

		return ob_get_clean();

	}

	public function generate_readonly_html( $key, $data ) {
		$field_key = $this->get_field_key( $key );
		$defaults  = array(
			'title'             => '',
			'disabled'          => true,
			'class'             => '',
			'css'               => '',
			'placeholder'       => '',
			'type'              => 'text',
			'desc_tip'          => false,
			'description'       => '',
			'custom_attributes' => array(),
		);

		$data = wp_parse_args( $data, $defaults );

		ob_start();
		?>
        <div class="form-field plugincore-form-field">
            <label for="<?php echo esc_attr( $field_key ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
            <input class="input-text regular-input <?php echo esc_attr( $data['class'] ); ?>"
                   type="text"
                   style="<?php echo esc_attr( $data['css'] ); ?>"
                   value="<?php echo esc_attr( PluginCore::value_or_call($data['value']) ); ?>"
                   placeholder="<?php echo esc_attr( $data['placeholder'] ); ?>"
				<?php disabled( $data['disabled'], true ); ?>
				<?php echo $this->get_custom_attribute_html( $data ); // WPCS: XSS ok. ?>>
			<?php echo $this->get_description_html( $data ); // WPCS: XSS ok. ?>
        </div>
		<?php

		return ob_get_clean();
	}

	/**
	 * Get custom attributes.
	 *
	 * @param  array $data Field data.
	 * @return string
	 */
	public function get_custom_attribute_html( $data ) {
		$custom_attributes = array();

		if ( ! empty( $data['custom_attributes'] ) && is_array( $data['custom_attributes'] ) ) {
			foreach ( $data['custom_attributes'] as $attribute => $attribute_value ) {
				$custom_attributes[] = esc_attr( $attribute ) . '="' . esc_attr( $attribute_value ) . '"';
			}
		}

		return implode( ' ', $custom_attributes );
	}

	public function get_option($key) {
		return $this->values[$key];
	}

	/**
	 * Get HTML for descriptions.
	 *
	 * @param  array $data Data for the description.
	 * @return string
	 */
	public function get_description_html( $data ) {
		if ( true === $data['desc_tip'] ) {
			$description = '';
		} elseif ( ! empty( $data['desc_tip'] ) ) {
			$description = $data['description'];
		} elseif ( ! empty( $data['description'] ) ) {
			$description = $data['description'];
		} else {
			$description = '';
		}

		return $description ? '<p class="description">' . wp_kses_post( $description ) . '</p>' . "\n" : '';
	}

}