<?php

if(!$_GET['sale_id']) {

	echo '<div class="alert alert-warning">' . __('Unknown Sale') . '</div>';

} else {

	$option = get_option(PRR_Admin::$option);

	$http = new WP_HTTP();
	$resp = $http->get('https://api.gumroad.com/v2/sales/' . urlencode($_GET['sale_id']), [
		'headers' => [
			'Authorization' => 'Bearer ' . $option['gumroad_key']
		]
	]);

	$body = json_decode($resp['body'], true);

	$key = $body['sale']['license_key'];
	$product = $body['sale']['product_permalink'];

	?>
		<p>
			<?= __("We recommend using WordPress Repos in order to install, and keep your purchased plugins up to date. Once installed, it's really simple to add your license key to your website"); ?>
		</p>

		<div class="card step-card">
			<div class="card-body">
				<p><span class="badge badge-info step"><?= __('Step 1'); ?></span></p>
				<p><a target="_blank" href="https://invisibledragon.gitlab.io/wordpress-repos/"><?= __('Install WordPress Repos'); ?></a></p>
			</div>
		</div>

		<div class="card step-card my-4">
			<div class="card-body">
				<p><span class="badge badge-info step"><?= __('Step 2'); ?></span></p>
				<p>
					<?= __('If you haven\'t installed any of our plugins before, go to Settings -> Plugin Repos and add your personalized link'); ?>
				</p>
				<pre class="selectall"><code><?= admin_url('admin-ajax.php?action=wprepo&key=' . urlencode($key) . '&product=' . urlencode($product)); ?></code></pre>
				<p>
					<?= __('If you already have, navigate to our repo inside of your WordPress installation, choose "Manage", and then enter your license details:'); ?>
				</p>
				<table class="table">
					<tr>
						<th><?= __('Product'); ?></th>
						<td><?= esc_html($product); ?></td>
					</tr>
					<tr>
						<th><?= __('License Key'); ?></th>
						<td><?= esc_html($key); ?></td>
					</tr>
				</table>
			</div>
		</div>

        <hr/>

        <div class="card step-card my-4">
            <div class="card-body">
                <p><span class="badge badge-info step"><?= __('Alternative'); ?></span></p>
                <p>
                    <?= __('If you do not want automatic updates and management, you can download the zip file below'); ?>
                </p>
                <p>
                    <?php
                        $plugins = new WP_Query(array(
                            'post_type' => PRR_CPT_Plugin::$post_type,
                            'meta_query' => array(
                                array(
                                    'key' => 'product_code',
                                    'value' => $product
                                )
                            )
                        ));

                        $version = new WP_Query(array(
                            'post_type' => PRR_CPT_PluginVersion::$post_type,
                            'meta_query' => array(
                                array(
                                    'key' => 'plugin',
                                    'value' => $plugins->posts[0]->ID
                                )
                            ),
                            'order' => 'DESC',
                            'meta_key' => 'version',
                            'orderby' => 'meta_value',
                            'per_page' => 1
                        ));

                        $version = $version->posts[0];
                    ?>
                    <a href="<?= get_post_meta($version->ID, 'download', true); ?>" class="btn btn-info">
                        Download ZIP
                    </a>
                </p>
            </div>
        </div>

	<?php

}
