<?php

use InvisibleDragon\PluginCore\PluginCore;
use InvisibleDragon\PluginCore\Settings_Fields;

class PluginRepoWoocommerce {

	public static function activate() {
		add_action( 'woocommerce_payment_complete', [ static::class, 'process_order_licenses' ] );

		add_action( 'woocommerce_product_options_general_product_data', [ static::class, 'general_product_data' ] );
		add_action( 'woocommerce_process_product_meta', [ static::class, 'save_product_meta' ] );

		add_action( 'woocommerce_account_wprepo_endpoint', [ static::class, 'account_view' ] );
		add_filter( 'woocommerce_account_menu_items', [ static::class, 'account_menu_item' ] );
		add_rewrite_endpoint( 'wprepo', EP_ROOT | EP_PAGES );

		add_filter( 'plugincore_query_woo_product', [ static::class, 'get_field_query' ], 10, 2 );

		add_filter( 'woocommerce_order_actions', [ static::class, 'order_actions' ] );
		add_action( 'woocommerce_order_action_pluginrepo_license', [ static::class, 'process_order_licenses' ] );

		add_action( 'woocommerce_order_status_completed', [ static::class, 'process_order_licenses' ] );
		add_action( 'woocommerce_order_status_processing', [ static::class, 'process_order_licenses' ] );

		add_action( 'woocommerce_order_details_before_order_table', [ static::class, 'order_before' ], 10 );
		add_action( 'woocommerce_order_item_meta_end', [ static::class, 'order_item_end' ], 10, 2 );

	}

	public static function order_before($order) {

		$products = $order->get_items();

		$show_section = false;

		foreach($products as $prod) {
			if ( $prod['product_id'] ) {
				$plugins = get_post_meta( $prod['product_id'], 'plugins', true );
				if ( $plugins ) {
					$show_section = true;
				}
			}
		}

		if($show_section) {
			echo '<div class="alert alert-info"><p>';
			echo __('You can find your license key and information from your My Account page');
			echo '</p><a href="' . esc_url(wc_get_account_endpoint_url('wprepo')) . '" class="btn btn-primary btn-sm">' . __('My Account') . '</a>';
			echo '</div>';
		}

	}

	public static function order_item_end($item_id, $item) {

		$plugins = get_post_meta( $item['product_id'], 'plugins', true );
		if($plugins) {

			echo '<div class="pluginrepo-item-information">';
			echo __('You can find your license key and information from your My Account page');
			echo '</div>';

		}

	}

	public static function order_actions($actions) {

		$actions['pluginrepo_license'] = __('Issue licenses for order');
		return $actions;

	}

	private static function get_fields() {
		return [
			'plugins' => [
				'type' => 'posts',
				'query' => [
					'post_type' => PRR_CPT_Plugin::get_post_type()
				],
				'title' => __('Plugins'),
				'description' => __('Plugins which access is granted to the customer by purchasing')
			]
		];
	}

	public static function get_field_query( $result, $key ) {

		$fields = static::get_fields();
		if($fields[$key]) {
			return $fields[$key]['query'];
		}
		return $result;

	}

	public static function general_product_data() {

		global $post;
		$product = wc_get_product($post->ID);
		$data = [
			'plugins' => $product->get_meta('plugins')
		];

		PluginCore::add_admin_css();
		PluginCore::add_admin_js();

		echo '<div class="options_group">';
		$settings = new Settings_Fields(static::get_fields(), $data);
		$settings->set_auth_path('woo_product');
		$settings->generate_settings_html();
		echo '</div>';

	}

	public static function save_product_meta($post_id) {

		$product = wc_get_product($post_id);

		$settings = new Settings_Fields(static::get_fields());
		$data = $settings->get_values($_POST);

		foreach($data as $key => $value) {
			$product->update_meta_data($key, $value);
		}
		$product->save();

	}

	public static function account_menu_item( $menu_links ) {

		$new = [
			'wprepo' => __('Plugins')
		];

		$menu_links = array_slice( $menu_links, 0, 1, true )
		              + $new
		              + array_slice( $menu_links, 1, NULL, true );

		return $menu_links;

	}

	public static function account_view() {

		require_once dirname(__FILE__) . '/templates/customer-account.php';

	}

	public static function process_order_licenses( $order_id ) {

		$order = wc_get_order( $order_id );
		$products = $order->get_items();

		if(!$order->get_customer_id()) {
			return; // Don't bother if anon order right now
		}

		foreach($products as $prod){
			if($prod['product_id']) {
				$plugins = get_post_meta( $prod['product_id'], 'plugins', true );
				foreach($plugins as $plugin) {
					// Check first if license already exists
					$query = new WP_Query([
						'post_type' => PRR_CPT_PluginLicense::get_post_type(),
						'post_author' => $order->get_customer_id(),
						'meta_query' => [
							[
								'key' => 'plugin',
								'value' => $plugin
							],
							[
								'key' => 'notes',
								'value' => 'Order#' . $order->get_id()
							]
						]
					]);
					if($query->post_count > 0) {
						continue; // Skip to next one if already exists!
					}
					// Now create one
					$key = \WordPressPluginRepo\Ramsey\Uuid\Uuid::uuid4()->toString();
					wp_insert_post( [
						'post_type'   => PRR_CPT_PluginLicense::get_post_type(),
						'post_status' => 'publish',
						'post_author' => $order->get_customer_id(),
						'meta_input'  => [
							'key'     => $key,
							'plugin'  => $plugin,
							'uses'    => $prod->get_quantity(),
							'notes'   => 'Order#' . $order->get_id()
						]
					] );
					$order->add_order_note('License ' . $key . ' created for ' . get_the_title($plugin));
				}
			}
		}

	}

}
