<h2><?= __('My Plugin Licenses'); ?></h2>

<h2><?= __('Recommended Installation'); ?></h2>
<p>
   <?= __('This method uses WordPress Repos as a way to interact between your web server and our web server and allows for updates to be sent correctly.'); ?>
</p>
<ul>
    <li>
        <a href="https://invisibledragon.gitlab.io/wordpress-repos/">
            <?= __('Install WordPress Repos to your WordPress installation'); ?>
        </a>
    </li>
    <li>
        <?= sprintf( __('Add %s to the list of repos, and then when prompted Authorize your website'), '<code>' . home_url() . '</code>'); ?>
    </li>
    <li>
        <?= __('Choose to Manage your authorized licenses, and add one of the license keys. When returning to your WordPress installation, you should see the plugin available to install'); ?>
    </li>
</ul>

<h2><?= __('My License Keys'); ?></h2>

<?php
$query = new WP_Query([
	'post_type' => PRR_CPT_PluginLicense::get_post_type(),
	'post_author' => get_current_user_id(),
    'per_page' => -1
]);

if($query->have_posts()):

?>
<table class="table table-striped">
    <thead>
        <tr>
            <th><?= __('Plugin Name'); ?></th>
            <th><?= __('Site Count'); ?></th>
            <th><?= __('License Key'); ?></th>
            <!--<th><?= __('Manual Download'); ?></th>-->
        </tr>
    </thead>
    <tbody>
        <?php foreach($query->posts as $post): ?>
        <tr>
            <td>
                <?= esc_html(get_the_title(get_post_meta( $post->ID, 'plugin', true ))); ?>
                <br/>
	            <?= esc_html(get_post_meta( $post->ID, 'notes', true )); ?>
            </td>
            <td>
		        <?= esc_html(get_post_meta( $post->ID, 'uses', true )); ?>
            </td>
            <td>
		        <?= esc_html(get_post_meta( $post->ID, 'key', true )); ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
else: ?>
<div class="alert alert-info">
    <?= __('You do not have any license keys attached to your account'); ?>
</div>
<?php endif; ?>
